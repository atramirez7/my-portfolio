import { Col, Container, Row } from "react-bootstrap";
import { ProjectCard } from "./ProjectCards";
import img1 from "../assets/image/NN1.png"
import img2 from "../assets/image/NN2.png"
import img3 from "../assets/image/NN3.png"
import img4 from "../assets/image/WC1.png"
import img5 from "../assets/image/WC2.png"
import img6 from "../assets/image/WC3.png"

export const Projects = () => {
    const projects = [
        {
            title: "Novice Network",
            description: "od tempor incididunt ",
            imgUrl: img1,
        },
        {
            title: "Novice Network",
            description: "od tempor incididunt ",
            imgUrl: img2,
        },
        {
            title: "Novice Network",
            description: "od tempor incididunt ",
            imgUrl: img3,
        },
        {
            title: "We Catered",
            description: "od tempor incididunt ",
            imgUrl: img4,
        },
        {
            title: "We Catered",
            description: "od tempor incididunt ",
            imgUrl: img5,
        },
        {
            title: "We Catered",
            description: "od tempor incididunt ",
            imgUrl: img6,
        },
    ];
    return (
        <section className="project" id="project">
            <Container>
                <Row>
                    <Col>
                    <h2>Projects</h2>
                    <p>description orem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad mini
                    </p>
                    <section>
                    <Row>
                        {projects.map((project, index) => {
                            return (
                            <ProjectCard
                                key={index}
                                {...project}
                             />
                            )
                        })}

                    </Row>
                    </section>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}
